const functions = require('firebase-functions');
const admin = require('firebase-admin');
const _ = require('lodash');

let db = admin.firestore();

/**
 * This function will register the user if they didn't already exist in the
 * databaes, and regardless of whether they were just created now, it will
 * return salient details as though they just logged in.
 */
exports.default = functions.https.onCall(async (data, context) => {
  // Ensure the user is actually has authentication information
  if (_.isNil(context) || _.isNil(context.auth)) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('unauthenticated');
  }

  const uid = context.auth.uid;

  // By the time the user has called into this function, they've authenticated
  // with Firebase, so we should be able to find their account. We do this so
  // that we can get their email address since we'll require that later in this
  // function. Not every identity provider includes the email address in the
  // decoded token, but if they include it at all, then Firebase will have it.
  const userRecord = await admin.auth().getUser(uid);
  if (_.isNil(userRecord)) {
    throw new functions.https.HttpsError('not-found');
  }

  // We require that all users have an email address so that we can contact them
  // if needed.
  const { email } = userRecord;
  if (_.isNil(email) || _.isEmpty(email)) {
    throw new functions.https.HttpsError('failed-precondition');
  }

  // Ensure that the user doesn't already have an account in our database
  const usersRef = db.collection('users');
  const querySnapshot = await usersRef.where('uid', '==', uid).get();

  if (querySnapshot.empty) {
    // This means that the user didn't exist, so we'll add them.
    const doc = {
      uid,
      purchasedCourses: [],
      createdAt: admin.firestore.Timestamp.now(),
    };
    await usersRef.add(doc);
    return doc;
  } else {
    const { docs } = querySnapshot;
    const firstDoc = _.head(docs);
    return firstDoc.data();
  }
});
