import React from 'react';
import PropTypes from 'prop-types';

/**
 * Simple, reusable button component.
 */
export default function Button({ children, text, ...htmlProps }) {
  return (
    <button {...htmlProps}>
      {text}
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node,
  text: PropTypes.node,
};
