import React from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Footer() {
  return (
    <div className="px-24 py-12 ">
      <footer className="text-gray-600 border-t-2 border-gray-100 pt-8  flex justify-between">
        <div>
          <ul className="flex">
            <li className="text-sm font-bold mr-4">
              <Link href="/" as={`/`}>
                <a>Privacy Policy</a>
              </Link>
            </li>
            <li className="text-sm font-bold mx-4">
              <Link href="/" as={`/`}>
                <a>Cookie Policy</a>
              </Link>
            </li>
            <li className="text-sm font-bold mx-4">
              <Link href="/" as={`/`}>
                <a>Terms of Service</a>
              </Link>
            </li>
            <li className="text-sm font-bold mx-4">
              <Link href="/" as={`/`}>
                <a>Returns Policy</a>
              </Link>
            </li>
            <li className="text-sm font-bold mx-4">
              <Link href="/" as={`/`}>
                <a>Support</a>
              </Link>
            </li>
            <li className="text-sm font-bold ml-4">
              <Link href="/" as={`/`}>
                <a>Contact</a>
              </Link>
            </li>
          </ul>

          <p className="text-sm mt-6">Copyright © 2020 AcAdamy</p>
        </div>
        <div className="flex">
          <a href="https://youtube.com/c/AdamLearns">
            <FontAwesomeIcon icon={['fab', 'youtube']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://gitlab.com/AdamLearnsShow">
            <FontAwesomeIcon icon={['fab', 'gitlab']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://twitch.tv/Adam13531">
            <FontAwesomeIcon icon={['fab', 'twitch']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://patreon.com/AdamLearns">
            <FontAwesomeIcon icon={['fab', 'patreon']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://discord.gg/AdamLearns">
            <FontAwesomeIcon icon={['fab', 'discord']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://instagram.com/adamlearnslive">
            <FontAwesomeIcon icon={['fab', 'instagram']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://twitter.com/AdamLearnsLive">
            <FontAwesomeIcon icon={['fab', 'twitter']} className="h-6 mr-6" />{' '}
          </a>
          <a href="https://facebook.com/AdamLearns">
            <FontAwesomeIcon icon={['fab', 'facebook']} className="h-6" />{' '}
          </a>
        </div>
      </footer>
    </div>
  );
}
