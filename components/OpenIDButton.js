import PropTypes from 'prop-types';
import React from 'react';
import Button from '../components/Button';
import classNames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * An OpenID button for something like "Log in with Google".
 */
export default function OpenIDButton({ iconColor, icon, text }) {
  return (
    <Button
      onClick={() => window.alert('I did not code this yet')}
      className="m-2 border w-22 h-22 flex flex-col p-4 items-center rounded-md border-gray-300"
    >
      <div>
        <FontAwesomeIcon
          className={classNames(iconColor, 'w-6 mb-2')}
          icon={icon}
        />
      </div>
      <div className="font-bold ">{text}</div>
    </Button>
  );
}

OpenIDButton.propTypes = {
  /**
   * Any valid CSS class, e.g. "text-red".
   */
  iconColor: PropTypes.string.isRequired,

  /**
   * Either a string like "coffee" or an array like ['fab', 'google'].
   */
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  text: PropTypes.node.isRequired,
};
