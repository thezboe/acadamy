import React from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Navbar() {
  return (
    <div className="bg-white shadow-lg py-3">
      <nav className="mx-32 text-indigo-700 flex  flex  align-middle items-center justify-between">
        <div>
          <ul className="flex align-middle items-center">
            <li className="">
              <Link href="/" as={`/`}>
                <a>
                  <img src="../svg/acadamy-logo.svg" className="h-12" />
                </a>
              </Link>
            </li>
            <li className="mx-8">
              <Link href="/courses" as={`/courses`}>
                <a className="text-lg font-bold flex align-middle items-center">
                  <FontAwesomeIcon icon={['fa', 'book']} className="h-4 mr-2" />
                  <span>Courses</span>
                </a>
              </Link>
            </li>
          </ul>
        </div>

        <ul className="flex align-middle items-center">
          <li>
            <Link href="/checkout" as={`/checkout`}>
              <a className="text-base font-bold flex align-middle items-center mr-6">
                <FontAwesomeIcon
                  icon={['fa', 'shopping-cart']}
                  className="h-4 mr-2"
                />
                My orders (2)
              </a>
            </Link>
          </li>
          <li>
            <Link href="/account" as={`/account`}>
              <a>
                <img
                  src="https://api.adorable.io/avatars/285/fadl@adorable.png"
                  className="rounded-full h-12 w-12 flex items-center justify-center border-2 border-indigo-700"
                />
              </a>
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
