module.exports = {
  theme: {
    extend: {
      width: {
        '22': '5.5rem',
        '80': '20rem',
        '100': '25rem',
        '132': '33rem',
      },
      fontFamily: {
        sans: ['Roboto'],
      },
      textColor: {
        googleRed: '#EA4335',
        twitchPurple: '#6441A5',
        patreonPeach: '#F96854',
        discordBlue: '#7289DA',
        twitterBlue: '#1DA1F2',
        gitlabOrange: '#FC6D26',
      },
    },
  },
};
