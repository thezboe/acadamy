import React, { useState, useEffect, useContext, createContext } from 'react';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';

const firebaseConfig = {
  apiKey: process.env.NEXT_STATIC_FIREBASE_API_KEY,
  authDomain: process.env.NEXT_STATIC_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.NEXT_STATIC_FIREBASE_DATABASE_URL,
  projectId: process.env.NEXT_STATIC_FIREBASE_PROJECT_ID,
  storageBucket: process.env.NEXT_STATIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.NEXT_STATIC_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.NEXT_STATIC_FIREBASE_APP_ID,
  measurementId: process.env.NEXT_STATIC_FIREBASE_MEASUREMENT_ID,
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
  firebase.functions().useFunctionsEmulator('http://localhost:5001');
}

const AuthContext = createContext();

// eslint-disable-next-line react/prop-types
export function AuthProvider({ children }) {
  const auth = useAuthProvider();

  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}

export const useAuth = () => {
  return useContext(AuthContext);
};

function useAuthProvider() {
  // Track the user in the local state.
  const [user, setUser] = useState(null);

  // TODO: make this generic with a provider enum
  async function registerWithTwitter() {
    var provider = new firebase.auth.TwitterAuthProvider();

    // To apply the default browser preference instead of explicitly setting it.
    firebase.auth().useDeviceLanguage();

    try {
      const result = await firebase.auth().signInWithPopup(provider);
      // This gives you a Google Access Token. You can use it to access the Google API.
      // var token = result.credential.accessToken;
      // The signed-in user info.
      var user = result.user;
      setUser(user);
      // ...
      console.log('Success');

      // var twitterProvider = new firebase.auth.TwitterAuthProvider();
      // const twitterResult = await firebase
      //   .auth()
      //   .currentUser.linkWithPopup(twitterProvider);

      console.log('Calling into loginOrRegister');
      const loginOrRegisterFn = firebase
        .functions()
        .httpsCallable('loginOrRegister');
      await loginOrRegisterFn();
      // const timestamp = new firebase.firestore.Timestamp(
      //   testResult.data.createdAt._seconds,
      //   testResult.data.createdAt._nanoseconds
      // );
    } catch (error) {
      // Handle Errors here.
      // var errorCode = error.code;
      // var errorMessage = error.message;
      // // The email of the user's account used.
      // var email = error.email;
      // // The firebase.auth.AuthCredential type that was used.
      // var credential = error.credential;
      // ...

      console.error('Error', error);
    }

    return user;
  }

  async function testeroni(param) {
    const testeroniFn = firebase.functions().httpsCallable('testeroni');
    const result = await testeroniFn({ name: param });
    return result.data.text;
  }

  async function logout() {
    await firebase.auth().signOut();

    setUser(null);
  }

  // Track any user auth stuff changes
  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
      } else {
        setUser(null);
      }
    });

    return () => unsubscribe();
  }, []);

  return {
    user,
    registerWithTwitter,
    testeroni,
    logout,
    // etc.
  };
}
