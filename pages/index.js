import React from 'react';
import Course from '../components/Course';
import Layout from '../components/Layout';

/**
 * This is just a placeholder.
 */
export default function Index() {
  return (
    <div className="">
      <Layout>
        <p>Hello world</p>
        <Course />
      </Layout>
    </div>
  );
}
