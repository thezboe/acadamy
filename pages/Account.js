// there is a weird bug on this page that makes the shadow on the navbar not show up correctly

import React from 'react';
import Link from 'next/link';
import Layout from '../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import OpenIDButton from '../components/OpenIDButton';

export default function Account() {
  return (
    <Layout>
      <div className="bg-gray-100">
        <div className="py-24">
          <div className="shadow rounded-lg bg-white max-w-3xl shadow mx-auto">
            <div className="shadow rounded-lg bg-white max-w-3xl shadow-lg mx-auto">
              <div className="bg-indigo-700 text-white py-8 flex align-middle items-center pl-8 rounded-t-lg mb-8">
                <FontAwesomeIcon
                  icon={['fa', 'user-circle']}
                  className="h-10 mr-8"
                />
                <div>
                  <h2 className="text-2xl">Account Information</h2>
                  <p className="text-sm text-indigo-200">
                    Edit your personal information and account connections.
                  </p>
                </div>
              </div>
              <div className="px-24 pb-10">
                <label className="font-bold text-lg">Connected Accounts</label>
                <div className="flex mt-4 mb-8">
                  <div className="bg-gray-100 rounded font-bold py-2 px-3 mx-1 flex flex-1 justify-between align-middle items-center ">
                    <div className="flex lign-middle items-center">
                      <FontAwesomeIcon
                        icon={['fa', 'check-circle']}
                        className="h-4 mr-2 text-green-400"
                      />
                      <FontAwesomeIcon
                        icon={['fab', 'twitch']}
                        className="h-4 mr-2 text-twitchPurple"
                      />
                      <span className="text-lg font-bold">Twitch</span>
                    </div>
                    <div className="flex lign-middle items-center">
                      <FontAwesomeIcon
                        icon={['fa', 'unlink']}
                        className="h-4 mr-2 text-twitchPurple"
                      />
                      <span className="text-twitchPurple">Disconnect</span>
                    </div>
                  </div>
                </div>

                <label className="font-bold text-lg">Add Another Service</label>
                <div className="flex flex-wrap mb-12">
                  <OpenIDButton
                    iconColor="text-googleRed"
                    icon={['fab', 'google']}
                    text="Google"
                  />
                  <OpenIDButton
                    iconColor="text-patreonPeach"
                    icon={['fab', 'patreon']}
                    text="Patreon"
                  />
                  <OpenIDButton
                    iconColor="text-discordBlue"
                    icon={['fab', 'discord']}
                    text="Discord"
                  />
                  <OpenIDButton
                    iconColor="text-twitterBlue"
                    icon={['fab', 'twitter']}
                    text="Twitter"
                  />
                  <OpenIDButton
                    iconColor="text-gitlabOrange"
                    icon={['fab', 'gitlab']}
                    text="GitLab"
                  />
                </div>
                <button
                  onClick={() => alert('Be careful!')}
                  className="flex align-middle items-center text-red-700 ml-auto font-bold"
                >
                  <FontAwesomeIcon
                    icon={['fa', 'trash']}
                    className="h-4 mr-2"
                  />
                  Delete your account{' '}
                </button>
              </div>
            </div>
          </div>

          {/* courses card */}
          <div className="shadow rounded-lg bg-white max-w-3xl shadow mx-auto mt-12">
            <div className="shadow rounded-lg bg-white max-w-3xl shadow-lg mx-auto">
              <div className="bg-indigo-700 text-white py-8 flex align-middle items-center pl-8 rounded-t-lg mb-8">
                <FontAwesomeIcon icon={['fa', 'book']} className="h-10 mr-8" />
                <div>
                  <h2 className="text-2xl">Courses and Credits</h2>
                  <p className="text-sm text-indigo-200">
                    View your purchases and manage course credits
                  </p>
                </div>
              </div>
              <div className="px-24 pb-10">
                <label className="font-bold text-lg">Purchased Courses</label>
                <table className="table-fixed text-sm font-bold my-4 border-b">
                  <thead className="bg-gray-800 text-white">
                    <tr className="text-left">
                      <th className="px-4 py-2 w-1/2 ">Course Title</th>
                      <th className="px-4 py-2 w-1/4">Purchase Date</th>
                      <th className="px-4 py-2 w-1/4">Cost</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="border-r border-l px-4 py-2 text-indigo-700">
                        <Link href="/" as={`/`}>
                          <a>Intro to CSS</a>
                        </Link>
                      </td>
                      <td className="border-r border-l px-4 py-2">
                        00/00/2020
                      </td>
                      <td className="border-r border-l px-4 py-2">$0.00</td>
                    </tr>
                    <tr>
                      <td className="border-r border-l px-4 py-2 text-indigo-700">
                        <Link href="/" as={`/`}>
                          <a>Some other course</a>
                        </Link>
                      </td>
                      <td className="border-r border-l px-4 py-2">
                        00/00/2020
                      </td>
                      <td className="border-r border-l px-4 py-2">$0.00</td>
                    </tr>
                    <tr>
                      <td className="border-r border-l px-4 py-2 text-indigo-700">
                        <Link href="/" as={`/`}>
                          <a>And another one</a>
                        </Link>
                      </td>
                      <td className="border-r border-l px-4 py-2">
                        00/00/2020
                      </td>
                      <td className="border-r border-l px-4 py-2">$0.00</td>
                    </tr>
                  </tbody>
                </table>

                <label className="font-bold text-lg">Course Credits</label>
                <p className="my-4">
                  You currently have <strong>$0</strong> of course credits to
                  spend toward a course.
                </p>
                <button
                  onClick={() => alert('Spend that money!')}
                  className="flex align-middle items-center text-indigo-700 font-bold"
                >
                  <FontAwesomeIcon
                    icon={['fa', 'wallet']}
                    className="h-4 mr-2"
                  />
                  Buy a course to redeem your credits.
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
